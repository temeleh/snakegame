onload = function() {
	canv = document.getElementById("bg");
	ctx = canv.getContext("2d");
	document.addEventListener("keydown", keyPush);
	document.addEventListener("touchend", resetGame);

	drawSettings();
	players.push(new Player());
	resetGame();
};

let mainLoop, ctx, canv, timerLoop;

let players = [];
let gameoverCount;

let cap, bonus, appleTouch, deletedTail = 0;
let contrlBindPlayer = 0, contrlBindId = "";

let ax, ay; //apple x and y
let appleR, appleG, appleB;

let aiGrid = [];

let appleEatSnd = new Audio("appleEat.wav");

function resetGame() {
	tc = gridSizes[gId];
	gs = 840 / tc;

	cap = Math.round(gs / 12);

	//init apple
    ax = Math.floor(Math.random() * tc);
    ay = Math.floor(Math.random() * tc);

    appleR = Math.floor(Math.random() * 210) + 45;
    appleG = Math.floor(Math.random() * 210) + 45;
    appleB = Math.floor(Math.random() * 210) + 45;

    let startPos = [{x:ax, y:ay}];
    let sx, sy;

    //init ai cells
    for (let i = 0; i < tc; i++) {
    	aiGrid[i] = [];
    	for (let j = 0; j < tc; j++) {
    		aiGrid[i][j] = new AiCell(i, j);
    	}
    }
	addNeighbors(aiGrid);

    //init players
    deletedTail = 0;
    bonus = false;
    for (let i = 0; i < players.length; i++) {
    	do {
    		sx = Math.floor(Math.random() * tc);
    		sy = Math.floor(Math.random() * tc);
    	} while (indexOfCoordsInTrail(sx, sy, startPos) != -1);

    	startPos.push({x:sx, y:sy});

    	players[i].px = sx;
        players[i].py = sy;

    	players[i].xv = 0;
    	players[i].yv = 0;

    	players[i].trail = [];
    	players[i].tail = 2;
    	players[i].growingTail = 0;

		players[i].canSwDir = true;
		players[i].nextDirX = undefined;
		players[i].nextDirY = undefined;

		players[i].gameover = false;
	}

	clearInterval(timerLoop);
	if (timerMaxTime != 0) {
		timerTime = timerMaxTime * 60;
		drawTimer();

		timerLoop = setInterval(drawTimer, 1000);
	} else {
		ctx.fillStyle = "white";
    	ctx.fillRect(1140, 0, 150, 50);
	}

	clearInterval(mainLoop);
    mainLoop = setInterval(game, 1000 / 10); //10fps = 1000ms / 10 interval
}

function game() { //main loop
	drawBG();

	updateAndDrawPlayers();
	checkPlayerCollisions(); //check if players collide

	drawShapesAndText();
	drawApple();

	if (gameoverCount == players.length) { // if all players are dead
		drawGameOverScreen(gameoverCount);
		return;
	}
	
	if (players.length > 1 && gameoverCount == players.length - 1) { // if only one player is still alive
		let aliveIndex = getAliveIndex(); // player index (0 - 4)
		let deadMostPointsIndex = getDeadMostPointsIndex();
		if (players[aliveIndex].tail > players[deadMostPointsIndex].tail) {
			drawGameOverScreen(gameoverCount);
		}
	}
}

function keyPush(evt) {
	if (contrlBindPlayer == 0) { // players 1-5, 0 isn't a player. 0 == not in binding mode.
		let code = evt.keyCode || evt.which;
		if (code == 13) { // enter key
			resetGame();
		} else {
			playersKeyPush : for (let i = 0; i < players.length; i++) {
        		switch (code) {
					case pSettings[i].cLeft:
        				if (players[i].xv != 1) turnPlayer(players[i], -1, 0);
        				break playersKeyPush;

					case pSettings[i].cUp:
						if (players[i].yv != 1) turnPlayer(players[i], 0, -1);
                    	break playersKeyPush;

                    case pSettings[i].cRight:
						if (players[i].xv != -1) turnPlayer(players[i], 1, 0);
                    	break playersKeyPush;

                    case pSettings[i].cDown:
						if (players[i].yv != -1) turnPlayer(players[i], 0, 1);
                    	break playersKeyPush;
        		}
        	}
		}
	} else { // change player control binding settings
		controlBinding(contrlBindPlayer, contrlBindId, evt.keyCode || evt.which);
	}
}

function turnPlayer(player, xv, yv) {
	if (player.canSwDir) {
		player.xv = xv; 
		player.yv = yv;

		player.nextDirX = undefined;
		player.nextDirY = undefined;
		player.canSwDir = false;
	} else {
		player.nextDirX = xv;
		player.nextDirY = yv;
	}
}

function indexOfCoordsInTrail(x, y, trail) {
	return trail.findIndex(function(e) {
		return e.x == x && e.y == y;
	});
}

function playSound(s) {
	switch(s) {
		case "apple":
			appleEatSnd.currentTime = 0;
			appleEatSnd.play();
			break;
	}
}

function getAliveIndex() {
	for (let i = 0; i < players.length; i++) {
		if (!players[i].gameover) {
			return i;
		}
	}
	return 0;
}

function getDeadMostPointsIndex() {
	let index = 0;
	for (let i = 0; i < players.length; i++) { // get first dead index
		if (players[i].gameover) {
			index = i;
			break;
		}
	}

	for (let i = index; i < players.length; i++) {
		if (players[i].gameover && players[i].tail > players[index].tail) {
			index = i;
		}
	}
	return index;
}

function addNeighbors(grid) {
	for (let i = 0; i < tc; i++) {
        for (let j = 0; j < tc; j++) {
			if (i < tc - 1) grid[i][j].neighbors.push(grid[i + 1][j]);
            if (i > 0) 		grid[i][j].neighbors.push(grid[i - 1][j]);
            if (j < tc - 1) grid[i][j].neighbors.push(grid[i][j + 1]);
            if (j > 0) 		grid[i][j].neighbors.push(grid[i][j - 1]);

            if (wrap) {
            	if (i == 0) 	 grid[i][j].neighbors.push(grid[tc - 1][j]);
            	if (j == 0) 	 grid[i][j].neighbors.push(grid[i][tc - 1]);
            	if (i == tc - 1) grid[i][j].neighbors.push(grid[0][j]);
            	if (j == tc - 1) grid[i][j].neighbors.push(grid[i][0]);
            }
        }
    }
}