# Multiplayer snake with an AI

HTML canvas multiplayer snake game with an AI that uses the A* pathfinding algorithm. The AI is optimized for multiplayer but isn't perfect, so it cannot get the maximum score possible. 

I started this project around 2016 and wrote the AI in 2017 but haven't made any big changes (except the documentation) after that. The project really is in need of reformatting because this was supposed to be only a small project but then it just grew. The global functions and variables everywhere make the code hard to debug and understand.

Hosted on heroku: https://aisnake.herokuapp.com

## How the AI works

Normally the A* search just searches the shortest path to the apple (using the manhattan distance as the heuristic) and the snake tries to follow that path. The search is restarted from scratch each frame. 

But that is not all. If the A* search succeeds, breadth first search is started from the apple. This flood fill search checks if there is enough space for the snake after A* path is followed, so the A* evaluated path is also taken into account. The breadth first search succeeds if it finds 1.5 * snake's length number of cells around the apple. The search succeeds also if it finds the snake's own tail starting from the apple because the snake can follow its tail endlessly (at least in single-player).

Only if the A* search and breadth first search both succeed, the ai player direction is set to point to the first cell in the A* search path. 

### Holding pattern

If the searches fail, the ai tries to follow its tail in a "holding pattern". The image shows the decision table for every 3x3 area near the snake's head. Purple cells show the possible directions the snake is allowed to go. The 22 lime green snakes show which patterns have a mirrored version, so there are 48 (2*22 + 4) patterns that have "predetermined allowed solutions". If there is only one purple cell in a pattern, the snake always goes to that cell when this pattern is encountered. 

Otherwise custom bfs searches are started from the purple cells to determine which purple route to take. If the searches connect the purple cells (find a path between the purple cells), the purple cell that has has less open neighboring cells is selected. When the searches don't connect, the purple cell is selected according to the search that finds the snake's tail, or if the tail cannot be found the search that contains more visited cells (is a longer path).

Cases when the snake cannot continue straight are not in this table because they are detected differently. If the snake has to turn left or right, the same search procedure is started, but without pattern matching so both directions are considered as potentially good directions.

![img](img/patterns.jpg)

The second image shows how the A* estimates paths that go near other players as longer paths. This is done to decrease the number of collisions between AI players. The black and grey cells show how the A* cell cost is affected near other players heads. Black cell "costs" 4 normal white cells, gray 3 and lightgray 2. 

The A* search is here from the red snake's point of view. The green snake is just another snake (human or ai). 

![img](img/astarCostVis.jpg)
