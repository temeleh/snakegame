////// Ai player logic //////
let AiCell = function(x, y) {
	this.i = x;
	this.j = y;

	this.f = 0;
	this.g = 0;
	this.cameFrom = null;

	this.neighbors = [];
	this.safeNeighbors = undefined;

	this.collidesPlayer = false;
}

AiCell.prototype.reset = function() {
    this.f = 0;
	this.g = 0;
	this.cameFrom = null;

	this.safeNeighbors = undefined;
	this.collidesPlayer = false;
}


let openSet = [];
let closedSet = [];
let lastCheckedCell = null;
let path;
let gotoCell;

function setAiDirection(player) {

	//let result = -1
	let result = evaluatePaths(player);
	// for (let i = 0; i < tc; i++) {
	// 	for (let j = 0; j < tc; j++) {
	// 		aiGrid[i][j].reset();
	// 	}
    // }
 	
    if (result == 1) { // found path
        path = getPath(lastCheckedCell);

        if (!isAppleSafe(path.slice(0, -1), player)) { // todo: optionally disable this check
            result = -1;
        } else {
            gotoCell = path[path.length - 2];
    
            moveToCell(player, gotoCell);

            return;
        }
    }
    
	if (result == -1) { // no path
		gotoCell = getHoldingPatternCell(player);

		moveToCell(player, gotoCell);
	}
}

function evaluatePaths(player) {
	closedSet = [];
	openSet = [aiGrid[player.px][player.py]];
	lastCheckedCell = aiGrid[player.px][player.py];

	for (let i = 0; i < tc; i++) {
		for (let j = 0; j < tc; j++) {
			aiGrid[i][j].reset();
		}
    }

	while (openSet.length > 0) {
		let best = 0; // highest f score index
		for (let i = 1; i < openSet.length; i++) {
			if (openSet[i].f < openSet[best].f) {
				best = i;
			} else if (openSet[i].f == openSet[best].f) { // resolve ties
            	if (openSet[i].g > openSet[best].g) { // longer path
					best = i;
					continue;
				}
				
				// less safe neighbors, closer to snakes, doesn't leave closed areas so easily
				if (openSet[i].g == openSet[best].g && getSafeNeighbors(openSet[i]).length < getSafeNeighbors(openSet[best]).length) {
					best = i;
				}

            }
		}
		let current = openSet[best];
		lastCheckedCell = current;

		if (current.i == ax && current.j == ay) {
			// path found
			return 1;
		}

		openSet.splice(best, 1);
		closedSet.push(current);

		for (let i = 0; i < current.neighbors.length; i++) {
			let neighbor = current.neighbors[i];
			if (closedSet.indexOf(neighbor) !== -1 || collidesWithPlayers(neighbor)) {
				continue; // already evaluated or a player
			}

			let tempG = current.g + getMovementCost(player, neighbor);
			if (openSet.indexOf(neighbor) === -1) {
				openSet.push(neighbor);
			} else if (tempG >= neighbor.g) {
				continue; // not a better path
			}

			neighbor.g = tempG;
			neighbor.f = neighbor.g + getManhattanDistance(neighbor.i, neighbor.j, ax, ay);
			neighbor.cameFrom = current;
		}
	} 
	// no solution
    return -1;
}

function getMovementCost(player, cell) {
    let maxDist = 4;
    let cost = 1;
    for (let i = 0; i < players.length; i++) {
		let dist;
		if (players[i] == player && player.trail.length > 0) { // own tail
			dist = getManhattanDistance(cell.i, cell.j, player.trail[0].x, player.trail[0].y);
		} else if (!players[i].gameover) { // other players head
            dist = getManhattanDistance(cell.i, cell.j, players[i].px, players[i].py);
		} else continue;

		if (dist <= maxDist) {
			cost += maxDist - dist;
		}
	}
	
	return cost;
}

function getManhattanDistance(x1, y1, x2, y2) {
	let dx = x2 - x1;
    let dy = y2 - y1;

    if (wrap) {
        dx = Math.min(Math.abs(dx), Math.abs(tc + dx), Math.abs(tc - dx));
        dy = Math.min(Math.abs(dy), Math.abs(tc + dy), Math.abs(tc - dy));
    }

    return Math.abs(dx) + Math.abs(dy);
}

function getPath(fromCell) {
	let temp = fromCell;
    let totalPath = [temp];
    while (temp.cameFrom) {
    	totalPath.push(temp.cameFrom);
    	temp = temp.cameFrom;
    }
    return totalPath;
}

function collidesWithPlayers(cell) {
	if (cell.collidesPlayer) return true;

	for (let i = 0; i < players.length; i++) {
		if (isNotSafeNeighbor(cell.i, cell.j, players[i])) {
			cell.collidesPlayer = true;
			return true;
		}
	}
	return false;
}

function collidesWithTail(cell) {
	for (let i = 0; i < players.length; i++) {
		if (players[i].trail.length > 0 && players[i].trail[0].x == cell.i && players[i].trail[0].y == cell.j) {
			return true;
		}
	}
	return false;
}

function getSafeNeighbors(cell) {
	if (cell.safeNeighbors != undefined) return cell.safeNeighbors;

	let safeN = cell.neighbors.slice(0);

	for (let i = safeN.length - 1; i >= 0; i--) {
		if (safeN[i].collidesPlayer) {
			safeN.splice(i, 1);
			continue;
		}
		for (let j = 0; j < players.length; j++) {
			if (isNotSafeNeighbor(safeN[i].i, safeN[i].j, players[j])) {
				safeN.splice(i, 1);
				break;
			}
		}
	}

	cell.safeNeighbors = safeN;
	return safeN;
}

function isNotSafeNeighbor(i, j, player) {
	let index = indexOfCoordsInTrail(i, j, player.trail);

	if (index == 0) { // tail
		return player.growingTail > 0;
	}

	return index != -1;
}

function getHoldingPatternCell(player) {
	let playerCell = aiGrid[player.px][player.py];
	let safeNeighbors = getSafeNeighbors(playerCell);

	if (safeNeighbors.length == 1) return safeNeighbors[0];
	if (player.trail.length == 0) return null;

	let playerDir = player.trail[player.trail.length - 1].dir; // directions 1 2 3 4, (left, straight, right, back)
	if (playerDir == 0) return null;

	let leftDir = changeDir(playerDir, -1);
	let rightDir = changeDir(playerDir, 1); 
	let safeDir; // directions -1 0 1, (left, straight, right)

	let L2Cell, R2Cell;

	if (safeNeighbors.length == 2) {
		if (safeNeighbors[0].i == safeNeighbors[1].i || safeNeighbors[0].j == safeNeighbors[1].j) { // top blocked
			return getSafestDirNeighbor(safeNeighbors, player);
		} else {
			L2Cell = getNeighborForDirection(leftDir, playerCell);
			R2Cell = getNeighborForDirection(rightDir, playerCell);

			if (L2Cell === safeNeighbors[0] || L2Cell === safeNeighbors[1]) {
				safeDir = -1;
			} else {
				safeDir = 1;
			}
		}
	} else if (safeNeighbors.length == 3) {
		L2Cell = getNeighborForDirection(leftDir, playerCell);
		R2Cell = getNeighborForDirection(rightDir, playerCell);

		safeDir = 0;
	} else return null;

	// get pattern
	let L2 = safeDir == 1 ? "1" : "0";
	let R2 = safeDir == -1 ? "1" : "0";

	let L1 = getStateForDirection(playerDir, L2Cell);
	let R1 = getStateForDirection(playerDir, R2Cell);

	let L3 = getStateForDirection(changeDir(leftDir, -1), L2Cell);
	let R3 = getStateForDirection(changeDir(rightDir, 1), R2Cell);

	let sol = getSolutionsForPattern(L1 + L2 + L3, R1 + R2 + R3); // String concat

	if (sol == undefined) {
		return null;
	}

	if (sol.length > 1) {
		let solCells = [];

		for (let i = 0; i < sol.length; i++) {
			if (sol[i] == 1) {
				solCells[i] = R2Cell;
			} else if (sol[i] == -1) {
				solCells[i] = L2Cell;
			} else {
				solCells[i] = getNeighborForDirection(playerDir, playerCell); 
			}
		}

		return getSafestDirNeighbor(solCells, player);
	}

	if (sol[0] == -1) {
		return L2Cell;
	} else if (sol[0] == 1) {
		return R2Cell;
	} else {
		return null;
	}
}

function getSolutionsForPattern(l, r) {
	let sol = patterns.find(function(e) {
		return e.L == l && e.R == r; // L == l, R == r
	});
	if (sol != undefined) {
		return sol.S.slice(0);
	}
	sol = patterns.find(function(e) {
		return e.L == r && e.R == l; // L == r, R == l
	});

	if (sol != undefined) {
		let invertSolS = sol.S.slice(0);
		invertSolS.forEach(function(v, i) {
			invertSolS[i] = 0 - v;
		});

		return invertSolS;
	}
	return undefined;
}


function getNeighborForDirection(dir, cell) {
	let i = cell.i;
	let j = cell.j;
	if (dir == 1) {
		i -= 1;
		if (i < 0) {i += tc; if (!wrap) return null;}
	} else if (dir == 2) {
		j -= 1;
		if (j < 0) {j += tc; if (!wrap) return null;}
	} else if (dir == 3) {
		i += 1;
		if (i >= tc) {i -= tc; if (!wrap) return null;} 
	} else {
		j += 1;
		if (j >= tc) {j -= tc; if (!wrap) return null;} 
	}
	return aiGrid[i][j];
}

function getStateForDirection(dir, cell) {
	if (cell == null) return "1";

	cell = getNeighborForDirection(dir, cell);
	return (cell == null || collidesWithPlayers(cell) ? "1" : "0");
}

function changeDir(origDir, offset) {
	let dir = origDir + offset;
	if (dir <= 0) {dir = 4} else if (dir >= 5) {dir = 1}
	return dir;
}

function getSafestDirNeighbor(sNeighbors, player) {
	if (sNeighbors == null) return null;
	if (sNeighbors.length == 1) return sNeighbors[0];

	let tailTouch; // which dir/neighbor touches tail
	let evalAreaTouch = [];

	let open = [];
	let visited = [];
	for (let i = 0; i < sNeighbors.length; i++) {
		open[i] = [sNeighbors[i]];
		visited[i] = [sNeighbors[i]];
    }

	let cNeighbors = sNeighbors.slice(0);
	while (checkIfContinues(cNeighbors, open, visited, tailTouch, evalAreaTouch, player)) {
		for (let i = 0; i < cNeighbors.length; i++) {
			if (open[i].length == 0) continue;
			let current = open[i].shift();
			if (current == null) continue;

			if (collidesWithTail(current)) {
				tailTouch = cNeighbors[i];
				break;
			}

			for (let j = 0; j < current.neighbors.length; j++) {
				let neighbor = current.neighbors[j];
				
				if (collidesWithTail(neighbor)) {
					tailTouch = cNeighbors[i];
					break;
				}

				let overlapIndex = indexOfAnotherEvalArea(neighbor, visited, i);
				if (overlapIndex != -1 && evalAreaTouch.length == 0) {
					evalAreaTouch = [i, overlapIndex];
					break;
				}

				if ((visited[i].indexOf(neighbor) == -1 && !collidesWithPlayers(neighbor)) || overlapIndex != -1) {
					open[i].push(neighbor);
					visited[i].push(neighbor);
				}
			}
		}
	}

	return cNeighbors[0];
}

function checkIfContinues(cNeighbors, open, visited, tailTouch, evalAreaTouch, player) {
	if (evalAreaTouch.length == 2 && cNeighbors.length > 1) {
		let less = evalAreaTouch[1];
		let more = evalAreaTouch[0];
		if (getSafeNeighbors(cNeighbors[evalAreaTouch[0]]).length < getSafeNeighbors(cNeighbors[evalAreaTouch[1]]).length && Math.random() > 0.3) {
			less = evalAreaTouch[0];
			more = evalAreaTouch[1];
		}

		open[less] = open[less].concat(open[more]);
		open.splice(more, 1);
		visited[less] = visited[less].concat(visited[more]);
		visited.splice(more, 1);

		cNeighbors.splice(more, 1);
		evalAreaTouch.length = 0;

		if (cNeighbors.length == 1) return false;
	}
	

	// else if (tailTouches.length > 0) { // only if is separate area!
	// 	let safeLength0 = getSafeNeighbors(tailTouches[0]).length;
	// 	cNeighbors[0] = tailTouches[0]; 
	// 	for (let t = 1; t < tailTouches.length; t++) {
	// 		let safeLengthT = getSafeNeighbors(tailTouches[t]).length;
	// 		if (safeLength0 > safeLengthT) {
	// 			safeLength0 = safeLengthT;
	// 			cNeighbors[0] = tailTouches[t];
	// 		}
	// 	}
	// 	//console.log("tailTouch length:" + tailTouches.length);
	// 	return false;
	// } 

	let emptyCount = 0;
	for (let i = cNeighbors.length - 1; i >= 0; i--) {
		if (open[i].length == 0) emptyCount++;

		if (visited[i].length > player.trail.length) {
			cNeighbors[0] = cNeighbors[i];
			return false;
		} 
	}

	if (emptyCount == cNeighbors.length)  {
		if (tailTouch != undefined) {
			cNeighbors[0] = tailTouch;
		} else {
			let best = 0;
			for (let i = 1; i < cNeighbors.length; i++) {
				if (visited[i].length > visited[best].length) {
					best = i;
				}
			}
			cNeighbors[0] = cNeighbors[best];
		}
		return false;
	}

	return true;
}

function indexOfAnotherEvalArea(neighbor, visited, ci) {
	for (let i = 0; i < visited.length; i++) {
		if (i == ci) continue;

		if (visited[i].indexOf(neighbor) != -1) {
			return i;
		}
	}
	return -1;
}

function isAppleSafe(evaluatedPath, player) {
	let open = [aiGrid[ax][ay]];
	let visited = [aiGrid[ax][ay]];

	while (open.length > 0) {
		let current = open.shift();
		for (let i = 0; i < current.neighbors.length; i++) {
			let neighbor = current.neighbors[i];

			if (collidesWithTail(neighbor)) {
				if (visited.length < 3) {
					open.push(neighbor);
					continue;
				}
				return true;
			}

			if (visited.indexOf(neighbor) == -1 && !collidesWithPlayers(neighbor) && !evaluatedPath.includes(neighbor)) {
				open.push(neighbor);
				visited.push(neighbor);

				if (visited.length > 1.5 * player.trail.length) {
					return true;
				}
			}
		}
	}

	return false;
}

function moveToCell(player, cell) {
	if (cell == null) return;

	player.xv = cell.i - player.px;
	player.yv = cell.j - player.py;

	if (player.xv == tc - 1) {
		player.xv = -1;
	} else if (player.xv == -tc + 1) {
		player.xv = 1;
	} else if (player.yv == tc - 1) {
		player.yv = -1;
	} else if (player.yv == -tc + 1) {
		player.yv = 1;
	}
}


