function drawApple() {
	if (appleTouch) {
		appleTouch = false;

		//set apple position
        for (let i = 0; i < 100; i++) {
        	ax = Math.floor(Math.random() * tc);
        	ay = Math.floor(Math.random() * tc);

        	let hasTail = false;
        	for (let i = 0; i < players.length; i++) {
        		if (indexOfCoordsInTrail(ax, ay, players[i].trail) != -1) {
        			hasTail = true;
        			break;
        		}
        	}
        	if (!hasTail) break;
        }

        if (Math.random() > 0.85) {
        	bonus = true;
        } else bonus = false;
	}

	if (bonus || deletedTail != 0) {
		ctx.fillStyle="rgb(" + Math.floor(Math.random() * 255) + ", " + Math.floor(Math.random() * 255) + ", " + Math.floor(Math.random() * 255) + ")";
		ctx.fillRect(ax * gs, ay * gs, gs, gs);
	} else {
		ctx.fillStyle="rgb(" + appleR + ", " + appleG + ", " + appleB + ")";
		ctx.fillRect(ax * gs + cap * 2 , ay * gs + cap * 2, gs - cap * 4, gs - cap * 4);
	}
}

function drawGameOverScreen(gameoverCount) {
	clearInterval(mainLoop);
	clearInterval(timerLoop);

	ctx.font = "bold 70px Arial";
	ctx.fillStyle = "rgb(255, " + appleG + ", " + appleB + ")";
	if (players.length == 1) ctx.fillText("GAME OVER", 175, 350);
	else {
		let winningPlayer = 0;
		if (gameoverCount == players.length) { //player who has most points wins
			let mostPoints = players[0].tail;
			winningPlayer = 1;
			for (let i = 1; i < players.length; i++) {
				if (players[i].tail > mostPoints) {
					mostPoints = players[i].tail;
					winningPlayer = i + 1;
				} else if (players[i].tail == mostPoints) {
					winningPlayer = 0;
				}
			}
		} else { //player who is still alive wins
			for (let i = 0; i < players.length; i++) {
				if (!players[i].gameover) {
					winningPlayer = i + 1;
					break;
				}
			}
		}

		if (winningPlayer == 0) ctx.fillText("TIE", 350, 350);
		else ctx.fillText("PLAYER " + winningPlayer + " WINS", 120, 350);
	}

	ctx.font = "bold 25px Arial";
	ctx.fillText("PRESS ENTER TO RESTART", 225, 450);
}

function drawBG() {
	if (gl) {
		ctx.fillStyle = "GRAY";
		ctx.fillRect(0, 0, 840, 840);
		ctx.fillStyle = bgColorBlack ? "black" : "white";

		for (let i = 0; i < tc; i++) {
			for (let j = 0; j < tc; j++) {
				ctx.fillRect(i * gs + cap, j * gs + cap, gs - cap * 2, gs - cap * 2);
			}
		}
	} else {
		let bg = bgColorBlack ? "black" : "white", border = bgColorBlack ? "white" : "black";
		if (wrap) {
			ctx.fillStyle = bg;
			ctx.fillRect(0, 0, 840, 840);
		} else {
			ctx.fillStyle = border;
            ctx.fillRect(0, 0, 840, 840);

            ctx.fillStyle = bg;
            ctx.fillRect(cap, cap, 840 - cap * 2, 840 - cap * 2);
		}
	}
}

function drawShapesAndText() {
	ctx.fillStyle = "gray";
	ctx.fillRect(840, 0, 125, 840); //player points bg
	ctx.fillRect(1762, 114, 42, 20); //grid size bg
	ctx.fillRect(1502, 608, 20, 150); //player settings num bg

	ctx.fillStyle = "black";
   	ctx.font = "bold 20px Arial";

	ctx.fillText(tc, 1764, 100 + 31);

	for (let i = 0; i < players.length; i++) {
		ctx.fillStyle = players[i].gameover ? "red" : "black";
		ctx.fillText("PLAYER " + (i + 1), 850, 100 + (i * 150));
		ctx.fillText((i + 1) + ".", 1502, 628 + (i * 30));
	}

    ctx.font = "bold 40px Arial";
	for (let i = 0; i < players.length; i++) {
		ctx.fillStyle = players[i].gameover ? "red" : "black";
		let points = players[i].tail - 2;
    	ctx.fillText(String(points), 889 - ((points == 0) ? 0 : Math.floor(Math.log10(points)) * 13), 150 * (i + 1));
	}
}

// function drawPlayerInfo(i) {
	
// }

function drawTimer() {
	ctx.fillStyle = "gray";
	ctx.fillRect(1140, 0, 150, 50);

	ctx.fillStyle = timerTime > 10 ? "black" : "red";
    ctx.font = "bold 20px Arial";

    ctx.fillText(getTimeString(timerTime), 1191, 32);

    if (timerTime == 0) {
    	drawGameOverScreen(players.length);
    }

    timerTime--;
}

function getTimeString(time) {
	let min = Math.floor(time / 60);
	let sec = time % 60;

	return min + ":" + ("0" + sec).slice(-2);
}


function drawSettings() {
	ctx.fillStyle = "gray";
	ctx.fillRect(1465, 0, 439, 840);

	let padding = 31;

	ctx.fillStyle = "black";
	ctx.font = "bold 20px Arial";

	ctx.fillText("PLAYER COUNT: ", 				1500, 100);
	ctx.fillText("GRID SIZE: ", 				1500, 100 + 	padding);
	ctx.fillText("BACKGROUND COLOR: ", 			1500, 100 + 2 * padding);
	ctx.fillText("SHOW GRID LINES: ", 			1500, 100 + 3 * padding);
	ctx.fillText("WRAP AROUND: ", 				1500, 100 + 4 * padding);
	ctx.fillText("EAT OTHERS' TAILS: ", 		1500, 100 + 5 * padding);
	ctx.fillText("EAT OWN TAIL: ", 			   	1500, 100 + 6 * padding);
	ctx.fillText("SAVE LOST TAIL IN APPLE: ", 	1500, 100 + 7 * padding);
	ctx.fillText("APPLE GIVES 1 POINT: ",		1500, 100 + 8 * padding);
	ctx.fillText("TIMER: ",						1500, 100 + 9 * padding);

	// ctx.fillText("AI SETTINGS: ",				1500, 100 +11 * padding);
	// ctx.fillText("IGNORE OTHERS' TAILS: ",		1500, 100 +12 * padding);
	// ctx.fillText("SEARCH WHOLE A* TREE: ",		1500, 100 +13 * padding);
	// ctx.fillText("SHOW A* TREE: ",				1500, 100 +14 * padding);


	ctx.fillText("PLAYERS' SETTINGS: ", 		1500, 100 +16 * padding);

	drawPlayerSettings();
}

function drawPlayerSettings() {
	let p1AiBox = document.getElementById("AiH1");
	let p1ColorBox = document.getElementById("Col1");
	for (let i = 0; i < colors.length; i++) {
		let option = document.createElement("option");
		option.text = colors[i];
		p1ColorBox.add(option);
	}
	p1ColorBox.value = pSettings[0].color;

	let p1CLeft = document.getElementById("cLeft1");
	let p1CUp = document.getElementById("cUp1");
	let p1CRight = document.getElementById("cRight1");
	let p1CDown = document.getElementById("cDown1");

	for (let i = 2; i <= 5; i++) {
		//ai/human
		initPlayerSettings(p1AiBox, "AiH", i);

		//color
		initPlayerSettings(p1ColorBox, "Col", i);

		//controls
		initPlayerSettings(p1CLeft, "cLeft", i);
		initPlayerSettings(p1CUp, "cUp", i);
		initPlayerSettings(p1CRight, "cRight", i);
		initPlayerSettings(p1CDown, "cDown", i);

	}
}

