let gridSizes = [8, 10, 12, 15, 20, 30, 40, 60];
let gId = 4;
let tc, gs;

let wrap = true;
let gl = false;
let bgColorBlack = true;
let tailEating = false;
let selfTailEating = false;
let tailSaving = false;
let appleOnePoint = false;
let aiIgnorePlayers = false;
let aiCheckWholeTree = false;
let aiShowTree = false;

let timerMaxTime = 0;
let timerTime = 0;

let pSettings = [
	new PlayerSettings("LIME", 37, 38, 39, 40),
	new PlayerSettings("CYAN", 65, 87, 68, 83),
	new PlayerSettings("YELLOW", 71, 89, 74, 72),
	new PlayerSettings("PURPLE", 76, 80, 222, 192),
	new PlayerSettings("PINK", 100, 104, 102, 101)
];

let colors = ["#DC143C", "RED", "PINK", "#FF69B4", "TOMATO", "ORANGE", "GOLD", "YELLOW", "PURPLE", "LIME", "GREEN", "CYAN", "#00BFFF", "BLUE", "MAROON"];

let keyCodeStrLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
let keyCodeStrNumMap = [
	{s:"LT", n:37},
	{s:"UP", n:38},
	{s:"RT", n:39},
	{s:"DN", n:40},

	{s:"N1", n:97},
	{s:"N2", n:98},
	{s:"N3", n:99},
	{s:"N4", n:100},
	{s:"N5", n:101},
	{s:"N6", n:102},
	{s:"N7", n:103},
	{s:"N8", n:104},
	{s:"N9", n:105}
];

// AI holding pattern decision table patterns:
var patterns = [
	// All available symmetric:
	{L:"000", R:"000", S:[-1, 1]},
	{L:"001", R:"001", S:[-1, 1]},
	{L:"100", R:"100", S:[-1, 0, 1]},
	{L:"101", R:"101", S:[-1, 0, 1]},

	// All available non-symmetric:
	{L:"100", R:"000", S:[-1, 0]},
	{L:"001", R:"000", S:[-1]},
	{L:"101", R:"000", S:[-1, 0]},
	{L:"001", R:"100", S:[-1, 1]},
	{L:"001", R:"101", S:[-1, 1]},
	{L:"100", R:"101", S:[-1, 0, 1]},

	// Left/(Right) blocked (non-symmetric):
	{L:"010", R:"000", S:[0, 1]},
	{L:"110", R:"000", S:[0, 1]},
	{L:"011", R:"000", S:[1]},
	{L:"010", R:"100", S:[0, 1]},
	{L:"010", R:"001", S:[0, 1]},
	{L:"111", R:"000", S:[0]},

	{L:"011", R:"100", S:[0, 1]},
	{L:"011", R:"001", S:[0, 1]},
	{L:"110", R:"100", S:[0, 1]},
	{L:"110", R:"001", S:[0, 1]},
	{L:"010", R:"101", S:[0, 1]},
	{L:"011", R:"101", S:[0, 1]},
	
	{L:"110", R:"101", S:[0, 1]},	
	{L:"111", R:"001", S:[1]},
	{L:"111", R:"100", S:[0, 1]},	
	{L:"111", R:"101", S:[0, 1]}
];




function incGrid() {
	document.getElementById("incgButton").blur();

	if (gId < gridSizes.length - 1) {
		gId++;
		resetGame();
	}
}

function decGrid() {
	document.getElementById("decgButton").blur();

	if (gId > 0) {
		gId--;
		resetGame();
	}
}

function toggle(id, bool) {
	let el = document.getElementById(id);
	el.blur();
	el.innerHTML = (bool ? "YES" : "NO");
}

function toggleWrap() {
	wrap = !wrap; toggle("wrapButton", wrap);
	resetGame();
}
function toggleGL() {
	gl = !gl; toggle("glButton", gl);
}
function toggleOtherEat() {
	tailEating = !tailEating; toggle("otherEat", tailEating);
}
function toggleSelfEat() {
	selfTailEating = !selfTailEating; toggle("selfEat", selfTailEating);
}
function toggleTailSaving() {
	tailSaving = !tailSaving; toggle("saveTail", tailSaving);
}
function toggleAppleOne() {
	appleOnePoint = !appleOnePoint; toggle("appleOne", appleOnePoint);
}
function toggleAiIgnore() {
	aiIgnorePlayers = !aiIgnorePlayers; toggle("aiIgnorePlayers", aiIgnorePlayers);
}
function toggleAiTreeCheck() {
	aiCheckWholeTree = !aiCheckWholeTree; toggle("aiCheckTree", aiCheckWholeTree);
}
function toggleAiShowTree() {
	aiShowTree = !aiShowTree; toggle("aiShowTree", aiShowTree);
}

function setPlayerCount() {
	let countBox = document.getElementById("pCount");
	countBox.blur();

	let count = countBox.value;

	players = [];
	for (let i = 0; i < count; i++) {
		players.push(new Player());
	}

	showPlayerSettings(count);

	resetGame();
}
function setBgColor() {
	let bgCol = document.getElementById("bgColor");
	bgCol.blur();

	if (bgCol.selectedIndex == 0) bgColorBlack = true;
	else bgColorBlack = false;
}
function setTimer() {
	let timer = document.getElementById("timer");
	timer.blur();

	timerMaxTime = timer.value;
	resetGame();
}




////// Players settings //////

function getKeyCodeNum(s) {
	let index = keyCodeStrLetters.indexOf(s);
	if (index != -1) {
		return index + 65;
	}

    for (let i = 0; i < keyCodeStrNumMap.length; i++) {
    	if (s === keyCodeStrNumMap[i].s) return keyCodeStrNumMap[i].n;
    }
    return s;
}

function getKeyCodeStr(n) {
	for (let i = 0; i < keyCodeStrNumMap.length; i++) {
    	if (n === keyCodeStrNumMap[i].n) return keyCodeStrNumMap[i].s;
    }
    if (n >= 65 && n <= 90) {
    	return keyCodeStrLetters[n - 65];
    }
    return n;
}

function initPlayerSettings(p1C, id, i) {
	let cont = p1C.cloneNode(true);
    cont.id = id + i;
    cont.style.visibility = "hidden";

    if (id === "AiH" || id === "Col") {
    	if (i == 2) cont.onchange = function() {setPlayerSettings(2);}; else
        if (i == 3) cont.onchange = function() {setPlayerSettings(3);}; else
        if (i == 4) cont.onchange = function() {setPlayerSettings(4);}; else
        			cont.onchange = function() {setPlayerSettings(5);};
    } else {
   		if (i == 2) cont.onclick = function() {initControlBinding(2, id);}; else
        if (i == 3) cont.onclick = function() {initControlBinding(3, id);}; else
        if (i == 4) cont.onclick = function() {initControlBinding(4, id);}; else
        			cont.onclick = function() {initControlBinding(5, id);};
    }

    if (id === "Col") {
    	cont.value = pSettings[i - 1].color;
    } else if (id === "cLeft") {
		cont.innerHTML = getKeyCodeStr(pSettings[i - 1].cLeft);
    } else if (id === "cUp") {
		cont.innerHTML = getKeyCodeStr(pSettings[i - 1].cUp);
    } else if (id === "cRight") {
		cont.innerHTML = getKeyCodeStr(pSettings[i - 1].cRight);
    } else if (id === "cDown") {
		cont.innerHTML = getKeyCodeStr(pSettings[i - 1].cDown);
    }

    cont.style.top = "" + (615 + ((i - 1) * 30));
    document.body.appendChild(cont);
}
function setPlayerSettings(playerNum) {
	//ai/human
	let aiHBox = document.getElementById("AiH" + playerNum);
	pSettings[playerNum - 1].aiPlayer = aiHBox.selectedIndex == 0;

	//color
	let colBox = document.getElementById("Col" + playerNum);
	pSettings[playerNum - 1].color = colBox.value;

	//controls
	let left = document.getElementById("cLeft" + playerNum);
	pSettings[playerNum - 1].cLeft = getKeyCodeNum(left.innerHTML);
	let up = document.getElementById("cUp" + playerNum);
	pSettings[playerNum - 1].cUp = getKeyCodeNum(up.innerHTML);
	let right = document.getElementById("cRight" + playerNum);
	pSettings[playerNum - 1].cRight = getKeyCodeNum(right.innerHTML);
	let down = document.getElementById("cDown" + playerNum);
	pSettings[playerNum - 1].cDown = getKeyCodeNum(down.innerHTML);
}
function showPlayerSettings(visCount) {
	for (let i = 2; i <= 5; i++) {
        document.getElementById("AiH" + i).style.visibility = i > visCount ? "hidden" : "visible";
        document.getElementById("Col" + i).style.visibility = i > visCount ? "hidden" : "visible";

        document.getElementById("cLeft" + i).style.visibility = i > visCount ? "hidden" : "visible";
        document.getElementById("cUp" + i).style.visibility = i > visCount ? "hidden" : "visible";
        document.getElementById("cRight" + i).style.visibility = i > visCount ? "hidden" : "visible";
        document.getElementById("cDown" + i).style.visibility = i > visCount ? "hidden" : "visible";
	}
}

function initControlBinding(i, id) {
	contrlBindPlayer = i;
	contrlBindId = id;
}

function controlBinding(i, id, newKeyCode) {
	let cont = document.getElementById(id + i);
	cont.blur();
	cont.innerHTML = getKeyCodeStr(newKeyCode);

	setPlayerSettings(i);
	contrlBindPlayer = 0;
}

