let Player = function() {
	this.px; // player x position
	this.py; // player y position

	this.xv; // x velocity
	this.yv; // y velocity

	this.trail;
	this.tail;
	this.growingTail; 

	this.canSwDir = true;
	this.nextDirX = undefined;
	this.nextDirY = undefined;

	this.gameover = false;
};

Player.prototype.updatePosition = function() {
	this.px += this.xv;
	this.py += this.yv;

	if (this.growingTail > 0) this.growingTail--;

	if (wrap) {
    	if (this.px < 0) {
    		this.px = tc - 1;
    	}
    	if (this.px > tc - 1) {
    		this.px = 0;
    	}
    	if (this.py < 0) {
    		this.py = tc - 1;
    	}
    	if (this.py > tc - 1) {
    		this.py = 0;
    	}
    } else if (this.px < 0 || this.px > tc - 1 || this.py < 0 || this.py > tc - 1) {
    	this.setGameover();
    }
};

Player.prototype.checkAppleTouch = function() {
	if (ax == this.px && ay == this.py) {
		appleTouch = true;
		playSound("apple");

		this.growingTail = this.tail;
		this.tail++;
    	if (!appleOnePoint && gId > 3) {
    		this.tail += gId - 3;
    	}

    	if (bonus || deletedTail != 0) {
    		this.tail += deletedTail == 0 ? 1 : deletedTail;
    		if (!appleOnePoint && gId > 3 && bonus) {
    			this.tail += (gId - 3) * (gId - 3);
    		}
    		deletedTail = 0;

    		appleR = Math.floor(Math.random() * 210) + 45;
    		appleG = Math.floor(Math.random() * 210) + 45;
    		appleB = Math.floor(Math.random() * 210) + 45;
    	} else {
    		let f = 10;
    		appleR += f;
    		appleG -= f;
    		appleB = Math.floor(Math.random() * 100);
		}
		this.growingTail = this.tail - this.growingTail;
    }

	if (this.tail > tc * tc) this.setGameover();

    this.canSwDir = true;
};

Player.prototype.draw = function() {
	//update direction
    let d = 0;
    if (this.xv == -1) {d = 1} else if (this.yv == -1) {d = 2} else if (this.xv == 1) {d = 3} else if (this.yv == 1) {d = 4}

    this.trail.push({x:this.px, y:this.py, dir:d});

    while (this.trail.length > this.tail) {
    	this.trail.shift();
    }

	//draw
	for (let i = 0; i < this.trail.length; i++) {
    	if (i == 0 || this.trail[i].dir == 0) {
    		ctx.fillRect(this.trail[0].x * gs + cap, this.trail[0].y * gs + cap, gs - cap * 2, gs - cap * 2);

    	} else if (this.trail[i].dir == 1) {
    		ctx.fillRect(this.trail[i].x * gs + cap, this.trail[i].y * gs + cap, gs, gs - cap * 2);
    		if (this.trail[i].x == tc - 1) {
    			ctx.fillRect(0, this.trail[i].y * gs + cap, cap, gs - cap * 2);
    		}
    	} else if (this.trail[i].dir == 2) {
    		ctx.fillRect(this.trail[i].x * gs + cap, this.trail[i].y * gs + cap, gs - cap * 2, gs);
    		if (this.trail[i].y == tc - 1) {
    			ctx.fillRect(this.trail[i].x * gs + cap , 0, gs - cap * 2, cap);
    		}
    	} else if (this.trail[i].dir == 3) {
    		ctx.fillRect(this.trail[i].x * gs - cap, this.trail[i].y * gs + cap, gs, gs - cap * 2);
    		if (this.trail[i].x == 0) {
    			ctx.fillRect(gs * tc - cap, this.trail[i].y * gs + cap, cap, gs - cap * 2);
    		}
    	} else {
    		ctx.fillRect(this.trail[i].x * gs + cap,this.trail[i].y * gs - cap, gs - cap * 2, gs);
    		if (this.trail[i].y == 0) {
    			ctx.fillRect(this.trail[i].x * gs + cap, gs * tc - cap, gs - cap * 2, cap);
    		}
		}

		//check self collision
		if (i < this.trail.length - 1 && this.trail[i].x == this.px && this.trail[i].y == this.py && this.tail > 2) {
       		ctx.fillStyle = "gray";
       		if (selfTailEating && !this.gameover) {
       			if (tailSaving) deletedTail += this.tail - (this.trail.length - i - 1);
       			this.tail = this.trail.length - i - 1;
       		} else {
       			this.setGameover();
       		}
        }
    }

    if (path != null) {
		// for (let i = 0; i < path.length; i++) {
		// 	ctx.fillRect(path[i].i * gs + cap * 3, path[i].j * gs + cap * 3, gs - cap * 6, gs - cap * 6);
		// }

//    ctx.fillStyle = "blue";
//    	for (let i = 0; i < openSet.length; i++) {
//    		ctx.fillRect(openSet[i].i * gs + cap, openSet[i].j * gs + cap, gs - cap * 2, gs - cap * 2);
//    	}
//    	ctx.fillStyle = "red";
//    	for (let i = 0; i < closedSet.length; i++) {
//    		ctx.fillRect(closedSet[i].i * gs + cap, closedSet[i].j * gs + cap, gs - cap * 2, gs - cap * 2);
//    	}
//    	ctx.fillStyle = "orange";
//    	for (let i = 0; i < path.length; i++) {
//    		ctx.fillRect(path[i].i * gs + cap, path[i].j * gs + cap, gs - cap * 2, gs - cap * 2);
//    	}


		// ctx.font = "12px Arial Black";
		// ctx.fillStyle = "gray";
		// for (let i = 0; i < aiGrid.length; i++) {
		// 	for (let j = 0; j < aiGrid[i].length; j++) {
		// 		if (aiGrid[i][j].f > 20) {
		// 			ctx.fillStyle = "red";
		// 			ctx.fillText(aiGrid[i][j].f, aiGrid[i][j].i * gs + 2 * cap, aiGrid[i][j].j * gs + 4 * cap);
		// 			ctx.fillStyle = "gray";
		// 		} else if (aiGrid[i][j].f > 10) {
		// 			ctx.fillStyle = "orange";
		// 			ctx.fillText(aiGrid[i][j].f, aiGrid[i][j].i * gs + 2 * cap, aiGrid[i][j].j * gs + 4 * cap);
		// 			ctx.fillStyle = "gray";
		// 		} else if (aiGrid[i][j].f > 5) {
		// 			ctx.fillStyle = "yellow";
		// 			ctx.fillText(aiGrid[i][j].f, aiGrid[i][j].i * gs + 2 * cap, aiGrid[i][j].j * gs + 4 * cap);
		// 			ctx.fillStyle = "gray";
		// 		} else {
		// 			ctx.fillText(aiGrid[i][j].f, aiGrid[i][j].i * gs + 2 * cap, aiGrid[i][j].j * gs + 4 * cap);
		// 		}
		// 	}
		// }
    }
};

Player.prototype.setGameover = function() {
	this.gameover = true;
	if (players.length > 1) {
		this.trail = [];
	}
}

function updateAndDrawPlayers() {
	gameoverCount = 0;

	if (Math.random() < 0.5) {
		for (let i = 0; i < players.length; i++) updatePlayer(i);
	} else {
		for (let i = players.length - 1; i >= 0; i--) updatePlayer(i);
	}
}

function updatePlayer(i) {
	if (!players[i].gameover) { //player is still alive
		if (pSettings[i].aiPlayer) setAiDirection(players[i]); //set direction for ai controlled player, (+-x or +-y velocity)
		else if (players[i].nextDirX != undefined && players[i].canSwDir) turnPlayer(players[i], players[i].nextDirX, players[i].nextDirY);

		players[i].updatePosition(); //update player position (px += xv, py += yv)

		ctx.fillStyle = pSettings[i].color; //set player color from settings
		players[i].draw(); //draw the player
		players[i].checkAppleTouch(); //check if player touches the apple
	} else { //player is not alive
		 gameoverCount++;
		 if (players.length == 1) {
			 ctx.fillStyle = pSettings[0].color;
			 players[0].draw();
		 }
	}
}

function checkPlayerCollisions() {
	let playersToKill = []; //list of players to kill after collision check

	for (let i = 0; i < players.length; i++) {
		for (let j = 0; j < players[i].trail.length; j++) {
			for (let k = 0; k < players.length; k++) {
				if (players[i] != players[k] && !players[k].gameover) { //no self collision and no gameover player px py check

					if (players[i].trail[j].x == players[k].px && players[i].trail[j].y == players[k].py) { //two players collide
						if (tailEating) { //tail eating
							if (j == players[i].trail.length - 2 || j == players[i].trail.length - 1) {
								if (tailSaving) deletedTail += (players[i].tail - 2) + (players[k].tail - 2)

                            	players[k].tail = 2;
                            	players[i].tail = 2;
                            } else {
                            	players[k].tail += players[i].tail - (players[i].trail.length - j - 1);
                            	players[i].tail = players[i].trail.length - j - 1;
                            }
						} else { //no tail eating
							playersToKill.push(players[k]);
						}
					}
				}
			}
		}
    }

    for (let i = 0; i < playersToKill.length; i++) {
    	playersToKill[i].setGameover();
    }
}

let PlayerSettings = function(c, cL, cU, cR, cD) {
	this.aiPlayer = true;

	this.color = c;

	this.cLeft = cL;
	this.cUp = cU;
	this.cRight = cR;
	this.cDown = cD;
}










































